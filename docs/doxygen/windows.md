##下载地址##

####doxygen主程序####
[http://www.stack.nl/~dimitri/doxygen/download.html](http://www.stack.nl/~dimitri/doxygen/download.html)

#### Graphviz####
windows需要安装绘图的扩展支持:
[http://www.graphviz.org/](http://www.graphviz.org/)


## 配置使用##
![Screenshot](../img/doxygen/1.png) 
![Screenshot](../img/doxygen/2.png) 
![Screenshot](../img/doxygen/3.png) 
![Screenshot](../img/doxygen/4.png) 
![Screenshot](../img/doxygen/5.png) 
![Screenshot](../img/doxygen/6.jpg) 
![Screenshot](../img/doxygen/7.png) 


## 配置项说明 ##

- EXTRACT_ALL 表示：输出所有的函数，但是private和static函数不属于其管制。
- EXTRACT_PRIVATE 表示：输出private函数。
- EXTRACT_STATIC 表示：输出static函数。同时还有几个EXTRACT，相应查看文档即可。
- HIDE_UNDOC_MEMBERS 表示：那些没有使用doxygen格式描述的文档（函数或类等）就不显示了。当然，如果EXTRACT_ALL被启用，那么这个标志其实是被忽略的。
- INTERNAL_DOCS 主要指：是否输出注解中的@internal部分。如果没有被启动，那么注解中所有的@internal部分都将在目标帮助中不可见。
- CASE_SENSE_NAMES 表示：是否关注大小写名称，注意，如果开启了，那么所有的名称都将被小写。对于C/C++这种字母相关的语言来说，建议永远不要开启。
- HIDE_SCOPE_NAMES 表示：域隐藏，建议永远不要开启。
- SHOW_INCLUDE_FILES 表示：是否显示包含文件，如果开启，帮助中会专门生成一个页面，里面包含所有包含文件的列表。
- INLINE_INFO ：如果开启，那么在帮助文档中，inline函数前面会有一个inline修饰词来标明。
- SORT_MEMBER_DOCS ：如果开启，那么在帮助文档列表显示的时候，函数名称会排序，否则按照解释的顺序显示。
- GENERATE_TODOLIST ：是否生成TODOLIST页面，如果开启，那么包含在@todo注解中的内容将会单独生成并显示在一个页面中，其他的GENERATE选项同。
- SHOW_USED_FILES ：是否在函数或类等的帮助中，最下面显示函数或类的来源文件。
- SHOW_FILES ：是否显示文件列表页面，如果开启，那么帮助中会存在一个一个文件列表索引页面。



### 示例代码 ###
```
#ifndef __DCS_DEPTH_H__
#define __DCS_DEPTH_H__

#include <opencv2/opencv.hpp>
#include "impl/DCSDepth.h"
#include "DCSVersion.h"
#include "Log.h"

#define DCS_DEPTH_FORMAT_YUV_NV21 0
#define DCS_DEPTH_FORMAT_YUV_YV12 1
#define DCS_DEPTH_FORMAT_BGR 2
#define DCS_DEPTH_FORMAT_RGB 3


////////////////////////////////////////////////////////////////////////////////////////////
//One input image data info.
//
//@param data               :image data
//@param width              :image width
//@param height             :image height
//@param format             :iamge format, now support YUV_NV21, YUV_YV12, BGR
//@param rotation           :image rotation. phone in portrait placed called 0 degree, round clockwise.
//@stride[2]:               :TODO
////////////////////////////////////////////////////////////////////////////////////////////
struct images_buffer_depth_t {
    unsigned char *data;

    int width;
    int height;
    int format;
    int rotation;
    int stride[2];
};


////////////////////////////////////////////////////////////////
//Init depth environment
//@param                       :null
//
//@return                     :0 for success other failure
////////////////////////////////////////////////////////////////
int dcs_depth_init();


/////////////////////////////////////////////////////////////////////////////////////////
//Generate disparity from the original main and sub image according to depth algorithm lib.
//
//@param main[IN]             :main camera input image
//@param sub[IN]              :sub camera  input image
//@param depth[OUT]           :output gray disparity map
//@return                     :0 for success other failure
/////////////////////////////////////////////////////////////////////////////////////////
int dcs_depth_generate(const images_buffer_depth_t *main, const images_buffer_depth_t *sub, images_buffer_depth_t *depth);

/////////////////////////////////////////////////////////////////////////////////////////
//Destroy depth resource
//
//@return                     :0 for success other failure
/////////////////////////////////////////////////////////////////////////////////////////
int dcs_depth_uninit();

/////////////////////////////////////////////////////////////////////////////////////////
//Get version info of depth lib
//
//@return                     :Return version info
/////////////////////////////////////////////////////////////////////////////////////////
const char* dcs_depth_getVersion();

/////////////////////////////////////////////////////////////////////////////////////////
//NOTE: Assistant method, just used for demo. It's easy to get bitmap data through android api
//Decode jpg to BGR format
//
//param path[IN]              :jpeg path
//param data[OUT]             :bgr image data
//
//@return                     :0 for success other failure
/////////////////////////////////////////////////////////////////////////////////////////
int dcs_depth_readJPG2BGR(const char* path, images_buffer_depth_t* data);

/////////////////////////////////////////////////////////////////////////////////////////
//NOTE: Assistant method, just used for demo.  It's easy to save bitmap data through android api
//encode bgr to jpg and save to file
//
//param path[IN]              :jpeg path
//param data[OUT]             :jpg image data
//
//@return                     :0 for success other failure
int dcs_depth_writeGray2JPG(const char* path, images_buffer_depth_t* data);

#endif
```




