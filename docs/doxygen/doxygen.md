**Doxygen是一种开源跨平台的，以类似JavaDoc风格描述的文档系统，完全支持C、C++、Java、Objective-C和IDL语言，部分支持PHP、C#。**

**注释的语法与Qt-Doc、KDoc和JavaDoc兼容。Doxygen可以从一套归档源文件开始，生成HTML格式的在线类浏览器，或离线的LATEX、RTF参考手册。**

**Windows,Linux,macos都有对应的版本支持**

##结构##
![Screenshot](../img/doxygen.jpg) 

##协议##
[GNU General Public License](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## 官网 ##
[http://www.stack.nl/~dimitri/doxygen/index.html](http://www.stack.nl/~dimitri/doxygen/index.html)

## 帮助手册 ##
[Chm下载](../soft/doxygen_manual-1.8.12.chm)