## 安装

```
apt-get update
apt-get install doxygen graphviz
```

## 生成默认的配置文件
```
doxygen -g 
```

## 进入源码目录执行
```
doxygen Doxyfile
```

## docker方式运行
```
#!/bin/bash

docker run --rm -it --name doxygen -v ${PWD}/www:/var/www/html -v ${PWD}/src:/doxygen/src/ registry.cn-hangzhou.aliyuncs.com/orzero/doxygen doxygen /doxygen/c-doxygen.conf
docker run --rm -it --name doxygen -v ${PWD}/www:/var/www/html -p 89:80 registry.cn-hangzhou.aliyuncs.com/orzero/doxygen
```

