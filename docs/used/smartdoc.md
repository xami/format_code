## smartdoc

[smartdoc](https://github.com/zhh77/smartdoc)，基于YUIDoc构建的Javascipt文档生成器

优点：
自动根据注释头整理成文档
分模块展示
注释内容支持markdown内容

注释编写说明： http://www.cnblogs.com/zhh8077/p/4011769.html

使用帮助： http://www.cnblogs.com/zhh8077/p/4010991.html

更加详细的注释帮助： http://yui.github.io/yuidoc/syntax/index.html

license : BSD

---demo---

我们内部正在用的：[sioeye2.0 API文档](http://10.120.10.55:1000)
配置文件：
```
module.exports = {
    //扫描的文件路径
    paths: ['routes/'],

    //demo读取路径
    demoDir:"demo/",

    //文档页面输出路径
    outdir: 'doc/apidoc/',

    //项目信息配置
    project: {

        //项目名称
        name: 'Sioeye2.0 API DOC',

        //项目描述，可以配置html，会生成到document主页
        description: '<h2>Sioeye2.0 DOC</h2> <p>服务器接口文档</p>',

        //版本信息
        version: '1.0.0',

        //地址信息
        url: 'index.html',
        //logo地址
        logo : 'assets/logo.png',
        //导航信息
        navs: [
            {
                name:"smartdoc文档注释demo",
                url:"https://github.com/zhh77/smartDoc"
            }
            ,{
            name: "About",
            url: "https://github.com/zhh77/smartjs"
        }]
    },

    //demo展示页面配置；需要加载的资源； 资源只能是css和js文件
    demo: {

        //外部资源链接
        link : ['http://code.jquery.com/jquery-1.11.0.min.js'],

        //文件复制路径; 将目下的资源复制到doc生成目录中，并在deom页面引用
        //paths : ['doc/theme-smart/assets/js/uicode.js','doc/apidoc/'],

        //是否开启在code编辑器中的自动完成功能(会将link和paths的引入加入)；默认开启；
        autoComplete : true
    },

    //自定义主题路径
    themedir: 'doc/theme-smart/',

    //自定义helpers
    helpers: ["doc/theme-smart/helpers/helpers.js"]
};
```
 
通过定时任务，来自动生成文档到demo目录，再通过nginx把demo目录输出
```
* * * * *  /scripts/smartdoc.sh
```

安装smartdoc支持：
```
npm install -g smartdoc
```

smartdoc.sh 内容：
```
cd 项目目录
git pull
/usr/local/bin/smartdoc
```
