## 自动文档整理手册

git clone https://xami@bitbucket.org/xami/format_code.git

### mkdocs手册编写说明

[http://markdown-docs-zh.readthedocs.io/zh_CN/latest/](http://markdown-docs-zh.readthedocs.io/zh_CN/latest/)

## Docker镜像

git clone https://xami@bitbucket.org/xami/docker-doxygen.git

# Doxygen Windows版本

### Graphviz图形处理库（windows版本）
[下载](soft/graphviz-2.38.msi)

### windows版本安装程序（windows版本）
[下载](soft/doxygen-1.8.12-setup.exe)

### doxygen_manual帮助手册
[doxygen_manual-1.8.12.chm](soft/doxygen_manual-1.8.12.chm)